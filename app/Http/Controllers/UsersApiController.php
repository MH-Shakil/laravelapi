<?php

namespace App\Http\Controllers;
use App\Http\Requests\UserStoreRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\User;
class UsersApiController extends Controller
{
   public function showUser($id=null){
       if ($id==''){
           $users = User::get();
           return response()->json(['users'=>$users],200);
       }else{
           $user = User::find($id);
           return response()->json(['user'=>$user],200);
       }
   }
   public function addUser(Request $request){
       if ($request->isMethod('post')){
           $data = $request->all();
           $rules = [
               'name'=>'required',
               'email'=>'required | email | unique:users',
               'password'=>'required',
           ];
           $customMessage =  [
               'name.required'=>'Name is required',
               'email.required'=>'Email is Required',
               'email.email'=>'Email Must be a valid email',
               'email.unique'=>'The email has been already taken',
           ];
           $validator = Validator::make($request->all(),$rules,$customMessage);
           if ($validator->fails()) {
               return response()->json($validator->errors(),422);
           }

           $user  = new User();
           $user->name = $request->name;
           $user->email = $request->email;
           $user->password =$request->password;
           $user->save();
           $message = "User successfully added";
           return response()->json(['message'=>$message],201);
       }else
           return response()->json(['message'=>'request no valid'],202);
   }
   public function addMultipleUser(Request $request){
//       return $request->all();
       if ($request->isMethod('post')){
           $data = $request->all();
           $rules = [
               'users.*.name'=>'required',
               'users.*.email'=>'required | email | unique:users',
               'users.*.password'=>'required',
           ];
           $customMessage =  [
               'users.*.name.required'=>'Name is required',
               'users.*.email.required'=>'Email is Required',
               'users.*.email.email'=>'Email Must be a valid email',
               'users.*.email.unique'=>'The email has been already taken',
           ];
           $validator = Validator::make($request->all(),$rules,$customMessage);
           if ($validator->fails()) {
               return response()->json($validator->errors(),422);
           }
           foreach($data['users'] as $adduser){
               $user  = new User();
               $user->name = $adduser['name'];
               $user->email =$adduser['email'];
               $user->password =bcrypt($adduser['password']);
               $user->save();
               $message = "User successfully added";
           }
           return response()->json(['message'=>$message],201);
       }else
           return response()->json(['message'=>'request no valid'],202);
   }
   public function updateUser(Request $request, $id){
//              return $request->all();
       if ($request->isMethod('put')){
           $data = $request->all();
           $rules = [
               'name'=>'required',
               'password'=>'required',
           ];
           $customMessage =  [
               'name.required'=>'Name is required',
               'password.required'=>'Password is required',

           ];
           $validator = Validator::make($data,$rules,$customMessage);
           if ($validator->fails()) {
               return response()->json($validator->errors(),422);
           }
               $user  = User::findOrfail($id);
               $user->name = $request->name;
               $user->password =$request->password;
               $user->update();
               $message = "User successfully Updated";
               return response()->json(['message'=>$message],201);
       }else
           return response()->json(['message'=>'request no valid'],202);
   }
   public function updateSingleRecord(Request $request,$id){
//       return $request->all();
       if ($request->isMethod('patch')){
           $data = $request->all();
           $rules = [
               'name'=>'required',
           ];
           $customMessage =  [
               'name.required'=>'Name is required',

           ];
           $validator = Validator::make($data,$rules,$customMessage);
           if ($validator->fails()) {
               return response()->json($validator->errors(),422);
           }
           $user  = User::findOrfail($id);
           $user->name = $request->name;
           $user->update();
           $message = "User successfully Updated";
           return response()->json(['message'=>$message],201);
       }else
           return response()->json(['message'=>'request no valid'],202);
   }
   public function deleteSingleRecord($id){
       User::find($id)->delete();
       $message = "Record has been deleted SuccessFully";
       return response()->json(['message'=>$message],200);
   }
    public function deleteMultipleRecord(Request $request){
       $header = $request->header('authorization');
       if ($header==''){
           $message = "authoraization is required";
           return response()->json(['message'=>$message],422);
       }else{
           if ($header == "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ik1heW51ZGRpbiBIYXNhbiIsImlhdCI6MTUxNjIzOTAyMn0.VWmHVnzuqfOm_ZkjnVzwhO3foizGlei56lkExTq4eyI")
           {
               if ($request->isMethod('delete')){
                   $data = $request->all();
                   User::whereIn('id',$data['ids']);
                   $message = "Record has been deleted SuccessFully";
                   return response()->json(['message'=>$message],200);
               }
           }else{
               $message = "Authorization in not match";
               return response()->json(['message'=>$message],422);
           }
       }

    }
}
