<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'required | email | unique:users',
            'password'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Name is required',
            'email.required'=>'Email is Required',
            'email.email'=>'Email Must be a valid email',
            'email.unique'=>'The email has been already taken',
        ];
    }
}
