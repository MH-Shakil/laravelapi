<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['name'=>'maynuddin','email'=>'maynuddinhsn@gmail.com','password'=>'123123'],
            ['name'=>'hasan','email'=>'hasan@gmail.com','password'=>'123123'],
            ['name'=>'shakil','email'=>'shakil@gmail.com','password'=>'123123'],
        ];
        User::insert($users);
    }
}
