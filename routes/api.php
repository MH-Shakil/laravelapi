<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
//show user route using get method api
Route::get('/users/{id?}',[\App\Http\Controllers\UsersApiController::class,'showUser']);
//adduser route for add a single user using post mehtod api
Route::post('/add-users',[\App\Http\Controllers\UsersApiController::class,'addUser']);
//add multiple user api for add multiple user using post mehtod api
Route::post('/add-multiple-user',[\App\Http\Controllers\UsersApiController::class,'addMultipleUser']);
//update user using put mehtod api
Route::put('/update-user/{id}',[\App\Http\Controllers\UsersApiController::class,'updateUser']);
//update single record , using fatch mehtod
Route::patch('update-single-record/{id}',[\App\Http\Controllers\UsersApiController::class,'updateSingleRecord']);
//delete single record api route using delete mehtod
Route::delete('delete-single-record/{id}',[\App\Http\Controllers\UsersApiController::class,'deleteSingleRecord']);
//delete multiple user route, using delete method
Route::delete('delete-multiple-record',[\App\Http\Controllers\UsersApiController::class,'deleteMultipleRecord']);
